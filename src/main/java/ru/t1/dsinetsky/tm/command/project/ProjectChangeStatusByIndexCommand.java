package ru.t1.dsinetsky.tm.command.project;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX;

    public static final String DESCRIPTION = "Changes status of project (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        final String newStatus = TerminalUtil.nextLine();
        final Status status = Status.toStatus(newStatus);
        final String userId = getUserId();
        final Project project = getProjectService().changeStatusByIndex(userId, index, status);
        showProject(project);
        System.out.println("Status successfully changed");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
