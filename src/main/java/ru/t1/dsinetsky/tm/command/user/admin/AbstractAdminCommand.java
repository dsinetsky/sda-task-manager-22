package ru.t1.dsinetsky.tm.command.user.admin;

import ru.t1.dsinetsky.tm.command.user.AbstractUserCommand;
import ru.t1.dsinetsky.tm.enumerated.Role;

public abstract class AbstractAdminCommand extends AbstractUserCommand {

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
