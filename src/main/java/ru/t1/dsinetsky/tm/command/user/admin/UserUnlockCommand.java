package ru.t1.dsinetsky.tm.command.user.admin;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractAdminCommand {

    public static final String NAME = TerminalConst.CMD_UNLOCK_USER;

    public static final String DESCRIPTION = "Unlocks user to allow his login";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        final String login = TerminalUtil.nextLine();
        getAuthService().unlockUserByLogin(login);
        System.out.println("User successfully unlocked!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
