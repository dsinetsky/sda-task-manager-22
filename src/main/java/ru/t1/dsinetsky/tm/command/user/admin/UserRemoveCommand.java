package ru.t1.dsinetsky.tm.command.user.admin;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractAdminCommand {

    public static final String NAME = TerminalConst.CMD_DELETE_USER;

    public static final String DESCRIPTION = "Deletes user from system";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeUserByLogin(login);
        System.out.println("User successfully removed!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
