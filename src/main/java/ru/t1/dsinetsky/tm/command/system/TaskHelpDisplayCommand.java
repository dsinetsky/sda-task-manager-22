package ru.t1.dsinetsky.tm.command.system;

import ru.t1.dsinetsky.tm.constant.TerminalConst;

public final class TaskHelpDisplayCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = null;

    public static final String NAME = TerminalConst.CMD_TASK_HELP;

    public static final String DESCRIPTION = "Shows task commands";

    @Override
    public void execute() {
        listCommands(getCommandService().getTaskCommands());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
