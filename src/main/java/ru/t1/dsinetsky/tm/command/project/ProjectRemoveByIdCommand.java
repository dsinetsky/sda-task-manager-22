package ru.t1.dsinetsky.tm.command.project;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public static final String NAME = TerminalConst.CMD_REMOVE_PROJECT_BY_ID;

    public static final String DESCRIPTION = "Removes project (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of project:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, id);
        System.out.println("Project successfully removed!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
