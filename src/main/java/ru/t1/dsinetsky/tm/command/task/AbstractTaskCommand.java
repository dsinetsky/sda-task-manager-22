package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected void showListTask(List<Task> listTasks) {
        System.out.println("List of tasks:");
        int i = 1;
        for (final Task task : listTasks) {
            System.out.println(i + ". " + task.toString());
            i++;
        }
    }

    protected void showTask(Task task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDesc());
        System.out.println("Status: " + Status.toName(task.getStatus()));
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
