package ru.t1.dsinetsky.tm.command.system;

import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.constant.VersionConst;

public final class VersionDisplayCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = ArgumentConst.CMD_VERSION;

    public static final String NAME = TerminalConst.CMD_VERSION;

    public static final String DESCRIPTION = "Shows program version";

    @Override
    public void execute() {
        System.out.println("Version: " + VersionConst.VERSION);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
