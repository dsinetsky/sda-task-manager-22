package ru.t1.dsinetsky.tm.command.system;

import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;

public final class HelpDisplayCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = ArgumentConst.CMD_HELP;

    public static final String NAME = TerminalConst.CMD_HELP;

    public static final String DESCRIPTION = "Shows all commands";

    @Override
    public void execute() {
        listCommands(getCommandService().getTerminalCommands());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
