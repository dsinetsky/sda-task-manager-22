package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.api.service.ITestCreateService;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.User;

public class TestCreateService implements ITestCreateService {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    public TestCreateService(final IProjectService projectService, final ITaskService taskService, final IUserService userService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void createTest() throws GeneralException {
        userService.createTest();
        for (final User user : userService.returnAll()) {
            projectService.createTest(user);
            taskService.createTest(user);
        }
    }

}
