package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectTaskService {

    void bindProjectById(String userId, String projectId, String taskId) throws GeneralException;

    void unbindProjectById(String userId, String projectId, String taskId) throws GeneralException;

    Project removeProjectById(String userId, String projectId) throws GeneralException;

    Project removeProjectByIndex(String userId, int index) throws GeneralException;

}
