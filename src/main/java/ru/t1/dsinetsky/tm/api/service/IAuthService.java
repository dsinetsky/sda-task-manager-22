package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.GeneralUserException;
import ru.t1.dsinetsky.tm.model.User;

public interface IAuthService {

    User registry(String login, String password) throws GeneralException;

    void login(String login, String password) throws GeneralUserException;

    void logout();

    boolean isAuth();

    String getUserId() throws GeneralUserException;

    User getUser() throws GeneralException;

    void checkRoles(Role[] roles) throws GeneralException;

    void lockUserByLogin(String login) throws GeneralException;

    void unlockUserByLogin(String login) throws GeneralException;

}
