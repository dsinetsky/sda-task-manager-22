package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.AbstractUserOwnedModel;
import ru.t1.dsinetsky.tm.model.User;

import java.util.List;

public interface IAbstractUserOwnedService<M extends AbstractUserOwnedModel> extends IAbstractUserOwnedRepository<M> {

    List<M> returnAll(String userId, Sort sort) throws GeneralException;

    void createTest(User user) throws GeneralException;

}
