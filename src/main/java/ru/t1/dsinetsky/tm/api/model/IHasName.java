package ru.t1.dsinetsky.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
