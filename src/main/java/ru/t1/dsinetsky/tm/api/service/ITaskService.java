package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskService extends IAbstractUserOwnedService<Task> {

    Task create(String userId, String name) throws GeneralException;

    Task create(String userId, String name, String desc) throws GeneralException;

    List<Task> returnTasksOfProject(String userId, String projectId) throws GeneralException;

    Task updateById(String userId, String id, String name, String desc) throws GeneralException;

    Task updateByIndex(String userId, int index, String name, String desc) throws GeneralException;

    Task changeStatusById(String userId, String id, Status status) throws GeneralException;

    Task changeStatusByIndex(String userId, int index, Status status) throws GeneralException;

}
